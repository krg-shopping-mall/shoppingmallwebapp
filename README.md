# README #

A java web application for a shopping mall management.
#### Other project related informations are in misc folder.####

### What is this repository for? ###

Gyurme's personal trial into developing a shopping mall management web application.

### How do I get set up? ###

* It is java-maven-web app project developed using apache netbeans ide with glassfish 5 as deployment server
* So the easiest set-up option is to mimic the original configurations and environment.

### Contribution guidelines ###

* Kindly requested not to push your contribution if you change deployment configuration or related configuration.

### Who do I talk to? ###

* Repo owner or admin
