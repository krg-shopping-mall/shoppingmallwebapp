<%-- 
    Document   : branchAdminPage
    Created on : Sep 22, 2019, 2:22:21 PM
    Author     : Gyurme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin-${branch_name}</title>
    </head>
    <body>
        <%@include file="adminMenu.jsp" %>
        <div class="branch-admin-body">
            <div class="inventory-register-new">
                <h1 class="branch-admin-options-header">Register product from central db to current branch's db</h1>
                <label>Product Code</label>
                <input type="text">
                <label>Product Details</label>
                <textarea>The description of the product from cental db</textarea>    
            </div>
            <div class="inventory-add">
                <h1 class="branch-admin-options-header">Add to inventory</h1>
                <table align="center" border="1">
                    <tr>
                        <td>
                            <br>
                            <label>Product Code</label>
                            <input type="text" id="product_code">
                            <br>
                            <br>
                            <textarea cols="33" rows="10">Detailed Product Description of the product in the Product Code Field</textarea>
                        </td>
                        <td valign="top">
                            <table id="sales_recording_table">
                                <tr id="sales_recording_table_header_row">
                                    <th>Particulars
                                    </th>
                                    <th>Qty</th>
                                    <th>Rate</th>
                                    <th>Total</th>
                                </tr>
                                <tr id="sales_recording_table_items_row">
                                    <td>
                                        <input type="text">
                                    </td>
                                    <td>
                                        <input type="text" >
                                    </td>
                                    <td>
                                        <input type="text">
                                    </td>
                                    <td>
                                        <input type="text">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <input type="button" value="Submit" onclick="recordSale();"
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div><!--end of <div class="inventory-add">-->  
            <div class="inventory-remove">
                <h1 class="branch-admin-options-header">Remove from inventory</h1>
                <table align="center" border="1">
                    <tr>
                        <td>
                            <br>
                            <label>Product Code</label>
                            <input type="text" id="product_code">
                            <br>
                            <br>
                            <textarea cols="33" rows="10">Detailed Product Description of the product in the Product Code Field</textarea>
                        </td>
                        <td valign="top">
                            <table id="sales_recording_table">
                                <tr id="sales_recording_table_header_row">
                                    <th>Particulars
                                        <input type="hidden" id="loggedin_user_branch" value="${loggedInUserBranch}">
                                        <input type="hidden" id="loggedin_user_id" value="${loggedInUserId}">
                                        <input type="hidden" id="loggedin_user_hash" value="${loggedInUserHash}">
                                    </th>
                                    <th>Qty</th>
                                    <th>Rate</th>
                                    <th>Total</th>
                                </tr>
                                <tr id="sales_recording_table_items_row">
                                    <td>
                                        <input type="text">
                                    </td>
                                    <td>
                                        <input type="text" >
                                    </td>
                                    <td>
                                        <input type="text">
                                    </td>
                                    <td>
                                        <input type="text">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <input type="button" value="Submit" onclick="recordSale();"
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div><!--end of <div class="inventory-remove">-->
            <div class="inventory-add">
                <h1 class="branch-admin-options-header">Transfer to other branch</h1>
                <table align="center" border="1">
                    <tr>
                        <td>
                            <br>
                            <label>Reciepent Branch Code</label>
                            <input type="text" id="reciepent_branch_code">
                            <br>
                            <br>
                            <label>Product Code</label>
                            <input type="text" id="product_code">
                            <br>
                            <br>
                            <textarea cols="33" rows="10">Detailed Product Description of the product in the Product Code Field</textarea>
                        </td>
                        <td valign="top">
                            <table id="sales_recording_table">
                                <tr id="sales_recording_table_header_row">
                                    <th>Particulars
                                        <input type="hidden" id="loggedin_user_branch" value="${loggedInUserBranch}">
                                        <input type="hidden" id="loggedin_user_id" value="${loggedInUserId}">
                                        <input type="hidden" id="loggedin_user_hash" value="${loggedInUserHash}">
                                    </th>
                                    <th>Qty</th>
                                    <th>Rate</th>
                                    <th>Total</th>
                                </tr>
                                <tr id="sales_recording_table_items_row">
                                    <td>
                                        <input type="text">
                                    </td>
                                    <td>
                                        <input type="text" >
                                    </td>
                                    <td>
                                        <input type="text">
                                    </td>
                                    <td>
                                        <input type="text">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <input type="button" value="Submit" onclick="recordSale();"
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div><!--end of <div class="inventory-transfer">--> 
        </div>
        <input type="hidden" id="loggedin_user_type" value="${loggedInUserType}">
        <input type="hidden" id="loggedin_user_branch" value="${loggedInUserBranch}">
        <input type="hidden" id="loggedin_user_id" value="${loggedInUserId}">
        <input type="hidden" id="loggedin_user_hash" value="${loggedInUserHash}">
        
        <script src="JScripts/jQuery-3.4.1.min.js"></script>
        <script src="JScripts/branch_admin_script.js"></script>
    </body>
</html>
