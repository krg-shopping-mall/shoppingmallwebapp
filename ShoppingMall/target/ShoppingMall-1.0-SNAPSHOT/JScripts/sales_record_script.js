
let saleProductsList = [];
let jsonResponse;
let totalPurchaseSum = 0;
function recordSale(){
    var xmlhttp = new XMLHttpRequest();
    var url = "billing";
    var loggedinUser = document.getElementById('loggedin_cashier').value;
    var userBranch = document.getElementById('loggedin_user_branch').value;
    var loggedinHash = document.getElementById('loggedin_user_hash').value;
    var customerIdFieldVal = document.getElementById('customer_id').value;
    var customerId = "0";
    if(customerIdFieldVal === null){
        console.log("nothing in customer field");
    }
    else{
        console.log(customerIdFieldVal);
    }
    var tpinIdFieldVal = document.getElementById('tpin').value;
    var tpin = "0";
    if(tpinIdFieldVal === null){
        tpin = "0";
    }
    else{
        tpin = tpinIdFieldVal;
    }
    var productsListJson = JSON.stringify(saleProductsList);
    var postData = "request_name=record_sale&loggedin_cashier="+loggedinUser+"&loggedin_user_branch="+userBranch
                    +"&loggedin_user_hash="+loggedinHash+"&customer_id=0"+"&tpin=0"+"&products_and_qtys="+productsListJson;    
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            //console.log(this.responseText);
            console.log("customer Id is: ");
        }
        console.log("customer Id is: " + customerIdFieldVal);
    };
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(postData);
}

// Get the product code input field
var productCodeField = document.getElementById("product_code");

// Execute a function when the user releases a key on the keyboard
productCodeField.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    var productCode = productCodeField.value;
    // add the details of the product with product to the side table
    // First check if a <tbody> tag exists, add one if not
    if ($("#sales_recording_table tbody").length === 0) {
      $("#sales_recording_table").append("<tbody></tbody>");
    }
    
   // $("#sales_recording_table tbody").children().eq(0).children().eq(2).children().eq(0).val(productCode);
    getProductDetails(productCode);
    productCodeField.value = "";
    var userBranch = document.getElementById('loggedin_user_branch').value;
    console.log("user branch is: Gyurme");
  }
});

function getProductDetails(productCode){
    var response;
    console.log(productCode);
    var xmlhttp = new XMLHttpRequest();
    var url = "billing";
    var loggedinUser = document.getElementById('loggedin_cashier').value;
    var userBranch = document.getElementById('loggedin_user_branch').value;
    var loggedinHash = document.getElementById('loggedin_user_hash').value;
    var postData = "request_name=product_details&product_code="+productCode+"&loggedin_cashier="+loggedinUser
        +"&loggedin_user_branch="+userBranch+"&loggedin_user_hash="+loggedinHash;    
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            response = this.responseText;
            console.log("response : " + response);
            var productFoundResponse = JSON.parse(response);
            if(productFoundResponse.productFindMessage === "found"){
                var product = productCode; //this is done to prevent same object repeating in array. a js flaw
                product = new Object();
                product = productFoundResponse.productDetails;
                product.quantity = 1;
                saleProductsList.push(product);
                //console.log("object name is: " + saleProductsList[0].productId);
                if(productFoundResponse.productFindMessage == "found"){
                    // Append product to the table
                    $("#sales_recording_table tbody").append(
                        "<tr>" +
                          "<td class='st-id-td'></td>" +
                          "<td class='st-name-td'></td>" +
                          "<td class='st-qty-td'><input type='text' class='st-qty-input' size='3' maxlength='4' align='right' onkeyup='multiply(this);'></td>" +
                          "<td class='st-rate-td'></td>" +
                          "<td class='st-total-td'></td>"+
                        "</tr>"
                    );
                    var trIndex = saleProductsList.length; //to know the index of currently added tr in the table
                    console.log("Index is: " + trIndex);
                    $("#sales_recording_table tbody").children().eq(trIndex - 1).children().eq(0).html(product.productId);
                    $("#sales_recording_table tbody").children().eq(trIndex - 1).children().eq(1).html(product.productName);
                    $("#sales_recording_table tbody").children().eq(trIndex - 1).children().eq(2).children().eq(0).val(1);
                    $("#sales_recording_table tbody").children().eq(trIndex - 1).children().eq(3).html(product.price);
                    $("#sales_recording_table tbody").children().eq(trIndex - 1).children().eq(4).html(1 * product.price);
                    
                    calculateTotalPurchaseSum();
                }
                else{
                    //
                }
            }
            else{
                if(productFoundResponse.productFindMessage == "notfound"){
                    alert("Wrong product code or you are not logged in from this device");
                }
                else if(productFoundResponse == null){
                    alert("No response from server");
                }
                else{
                    alert("Unknown error occured");
                }
            }
        }
    };
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(postData);
}

function multiply(field){
    var qty = $(field).val();
    var productId = $(field).parent().parent().children().eq(0).text();
    var trIndex = $(field).parent().parent().index(); //index of tr among the trs of tbody
    var product = saleProductsList[trIndex];
    if(product.productId == productId){
        saleProductsList[trIndex].quantity = qty;
        var rate = $(field).parent().parent().children().eq(3).text();
        var total = qty * rate;
        $(field).parent().parent().children().eq(4).html(total.toFixed(2));
        
        calculateTotalPurchaseSum();
    }
    
}

//this is unnecessarily resource intense process
function calculateTotalPurchaseSum(){
    var sum =0;
    var count = $("#sales_recording_table tbody").children().length;
    for(i=0; i<count; i++){
        var currentAmount = Number($("#sales_recording_table tbody").children().eq(i).children().eq(4).text());
        sum = sum + currentAmount;
    }
    totalPurchaseSum = sum;
    $("#paymentProcessTable tbody").children().eq(0).children().eq(1).html(sum.toFixed(2));
    
}






