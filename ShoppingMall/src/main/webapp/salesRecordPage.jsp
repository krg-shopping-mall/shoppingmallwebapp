<%-- 
    Document   : salesRecordPage
    Created on : Sep 16, 2019, 8:03:35 AM
    Author     : Gyurme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.shoppingmall.models.Employee"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Record</title>
    </head>
    <body>
        <h1 align="center">Record Sales</h1>
        <table align="center" border="1">
            <tr>
                <td>
                    <br>
                    <label>Product Code</label>
                    <input type="text" id="product_code" size="8" maxlength="8">
                    <br>
                    <label>Customer</label>
                    <input type="text" id="customer_id" size="8" maxlength="8">
                    <br>
                    <label>TPIN</label>
                    <input type="text" id="tpin" size="8" maxlength="8">
                    <br>
                    <br>
                    <textarea cols="33" rows="10">Detailed Product Description of the product in the Product Code Field</textarea>
                </td>
                <td valign="top" align="right">
                    <table id="sales_recording_table" border="1">
                        <thead>
                            <tr id="sales_recording_table_header_row">
                                <th>ID
                                    <input type="hidden" id="loggedin_user_type" value="${loggedInUserType}">
                                    <input type="hidden" id="loggedin_cashier" value="${loggedInUserName}">
                                    <input type="hidden" id="loggedin_user_branch" value="${loggedInUserBranch}">
                                    <input type="hidden" id="loggedin_user_hash" value="${loggedInUserHash}">
                                </th>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Rate</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        
                    </table>
                                <br><br>
                                <table id="paymentProcessTable">
                                    <tbody align="right">
                                        <tr>
                                            <td>Total</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Cash handed</td>
                                            <td><input type="text" size="7"></td>
                                        </tr>
                                        <tr>
                                            <td>Return</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="right"><input type="button" value="Submit" onclick="recordSale();"></td>
                                        </tr>
                                    
                                    </tbody>
                                </table>
                                <br><br>
                </td>
            </tr>
        </table>
        <script src="JScripts/jQuery-3.4.1.min.js"></script>
        <script src="JScripts/sales_record_script.js"></script>
    </body>
</html>
