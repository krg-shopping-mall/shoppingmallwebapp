<%-- 
    Document   : cashierLogin
    Created on : Sep 19, 2019, 4:22:42 PM
    Author     : Gyurme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cashier Login</title>
    </head>
    <body>
        <h1>Welcome! Please Login</h1>
        <h3>${loginFailedMessage}</h3>
        <form id="cashier_login_form" method="post">
            <input type="hidden" name="request_name" value="${login_type}">
            <label>Branch Id</label>
            <input type="text" name="branch_id">
            <label>Username</label>
            <input type="text" name="username">
            <label>Password</label>
            <input type="text" name="password">
<!--            <c:if test="${login_type}!=main_admin"></c:if>branch code option if not main admin-->
            <input type="submit" id="button" value="Login">
            
        </form>
    </body>
</html>
