/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daos;

import com.shoppingmall.models.Product;
import com.shoppingmall.models.SalesMetaData;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public interface SalesDAO {
    //sales or invoice details. should return the created row-id (primary key)
    long recordSalesNonproductDetails(SalesMetaData salesData);
    
    //return sales metadata details
    SalesMetaData getSalesNonProductDetails(int branchId, long salesId);
    
    //for entering to db product details for each sales or invoice
    int recordSalesProductRecord(SalesMetaData smd, List<Product> productIdAndQty);
    
    //to get product details for each sales/invoice
    List<Product> productsAndQtysForSale(SalesMetaData smd);
    
    //List all sales meta since date with pagination
    List<SalesMetaData> allSalesMetaSince(String fromDate, int noOfRowsPerPage, long previouslyViewedTillRow);
}
