/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daos;

import com.shoppingmall.models.Product;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public interface InventoryDAO {
    //increase qtys of branch inventory by numbers provided from here
    int addToInventory(int branchId, List<Product> productIdsAndQtys);
    
    //increase qtys of branch inventory by numbers provided from here
    int decreaseFromInventory(int branchId, List<Product> productIdsAndQtys);
    
    //add to damaged list
    int addDamagedList(int branchId, List<Product> productIdsAndQtys);
    
    //add to branch transfer list
    int transferToOtherBranch(int senderBranchId, int reciepentBranchId, List<Product> productsIdQty);
}
