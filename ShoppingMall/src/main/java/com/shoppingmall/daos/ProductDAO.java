/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daos;

import com.shoppingmall.models.Product;

/**
 *
 * @author Gyurme
 */
public interface ProductDAO {
    //add category
    int addCategory(String Category);
    //edit category spelling and details
    int editCategory(long id, String Category);
    //add product
    int addProduct(Product pd);
    //edit product details
    int editProduct(long productId, Product pd);
    //get product details
    Product getProductDetails(int productId);
}
