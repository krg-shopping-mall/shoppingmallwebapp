/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daos;

import com.shoppingmall.models.Branch;
import java.sql.Connection;

/**
 *
 * @author Gyurme
 */
public interface AdminDAO {
    int recordNewBranchDetails(Branch branch, Connection conn);
    String createNewBranchTables(String branchId, Connection conn); //this for now is intended to return to user the sql queries
    //to create branch tables and relationships
}
