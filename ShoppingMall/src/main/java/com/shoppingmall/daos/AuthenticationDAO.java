/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daos;

/**
 *
 * @author Gyurme
 */
public interface AuthenticationDAO {
    
    String hashedPwdFromDb(String employee_id);
    
    int recordGeneratedLoginHashForUser(String branch, String userPosition, String username, String loginHash);
    
    String getUserLoggedinHash(String branch, String userPosition, String username);
}
