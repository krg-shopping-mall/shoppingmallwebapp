/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daos;

import com.shoppingmall.models.Employee;

/**
 *
 * @author Gyurme
 */
public interface EmployeeDAO {
    //add employee
    int addEmployee(Employee emp);
    //employee resignation
    int resignEmployee(long employeeId, String resignationDate);
    //assign branch
    int assignBranch(long employeeId, int branchId, String assignedDate);
    //remove from branch
    int removeFromBranch(long employeeId, int branchId, String removeDate);
    //change position
    int changePosition(String positionName);
    //edit details
}
