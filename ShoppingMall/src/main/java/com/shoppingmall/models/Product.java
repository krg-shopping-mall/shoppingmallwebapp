/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.models;

import java.math.BigDecimal;

/**
 *
 * @author Gyurme
 */
public class Product {
    private long productId = 0; // this is to check later in if conditions because the product ids start from 1
    private long categoryId;
    private String productName;
    private BigDecimal price; // BigDecimal instead of Double because of mysql mapping(Mysql float = java double,
                                //mysql decimal = java.math.BigDecimal)
    private BigDecimal quantity;
    private String quantityType; // kg, litre, count etc
    private String reason; // for sale return or removing from shelves

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(String quantityType) {
        this.quantityType = quantityType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
