/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.models;

/**
 *
 * @author Gyurme
 */
public class Branch {
    private int branchId;
    private String branchName;
    private String branchAddress;
    private int assigned_admin;

    public int getAssigned_admin() {
        return assigned_admin;
    }

    public void setAssigned_admin(int assigned_admin) {
        this.assigned_admin = assigned_admin;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }
    
}
