/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.models;

import java.util.Map;

/**
 *
 * @author gyurme
 */
public class Role {
    String roleId;
    String roleName;
    String roleDescription;
    Map <String, String> priviliges;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public Map<String, String> getPriviliges() {
        return priviliges;
    }

    public void setPriviliges(Map<String, String> priviliges) {
        this.priviliges = priviliges;
    }
}
