/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.models;

/**
 *
 * @author Gyurme
 */
public class Employee {
    private long employeeId;
    private String name;
    private long contactNo;
    private String joinDate;
    private String leaveDate;
    private long currentBranchId;
    private String userName;
    private String password;
    private String loggedInHash;

    public String getLoggedInHash() {
        return loggedInHash;
    }

    public void setLoggedInHash(String loggedInHash) {
        this.loggedInHash = loggedInHash;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getContactNo() {
        return contactNo;
    }

    public void setContactNo(long contactNo) {
        this.contactNo = contactNo;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }

    public long getCurrentBranchId() {
        return currentBranchId;
    }

    public void setCurrentBranchId(long currentBranchId) {
        this.currentBranchId = currentBranchId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
