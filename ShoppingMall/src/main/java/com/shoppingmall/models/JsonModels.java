/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.models;

import java.util.List;

/**
 *
 * @author Gyurme
 */
public class JsonModels {
    String productFindMessage; //either "found" or "notfound" while users look for products
    
    Product productDetails;

    public String getProductFindMessage() {
        return productFindMessage;
    }

    public void setProductFindMessage(String productFindMessage) {
        this.productFindMessage = productFindMessage;
    }

    public Product getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(Product productDetails) {
        this.productDetails = productDetails;
    }
    
    List<Product> productsList;

    public List<Product> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<Product> productsList) {
        this.productsList = productsList;
    }
}
