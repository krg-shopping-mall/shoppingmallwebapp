/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.apis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 *
 * @author Gyurme
 */
public class TrialJsonReqFromServlet {

    String serverIpAndPort;
    public String trialRequest(String requestString){
                serverIpAndPort = "192.168.100.72";//String.valueOf(OptionsPane.ipField.getText());

        HttpURLConnection http = null;
        String result = "";
        try {
            URL url = new URL("http://"+serverIpAndPort+"/gtransbackup/jsonhandler.php");
            http = (HttpURLConnection) url.openConnection();
            //http.setRequestProperty("trialKey","value");
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);

//            Uri.Builder builder = new Uri.Builder()
//                    .appendQueryParameter("reqId", requestString);
//            String query = builder.build().getEncodedQuery();

            OutputStream ops = http.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
            String data = URLEncoder.encode("reqId","UTF-8")+"="+URLEncoder.encode(requestString,"UTF-8");
            writer.write(data); //commented after putting it to web project
            //operationMessage = data;
            writer.flush();
            writer.close();
            ops.close();
            http.connect();

            InputStream ips = http.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(ips,"ISO-8859-1"));
            String line = "";
            while ((line = reader.readLine()) != null){
                result += line;
            }
            reader.close();
            ips.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(http != null) // Make sure the connection is not null.
                http.disconnect();
        }
        //operationMessage = result;
        return result;
    }
}
