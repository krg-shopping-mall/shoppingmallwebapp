/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.apis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shoppingmall.models.JsonModels;
import com.shoppingmall.models.Product;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public class JsonHandler {
    public List<Product> jsonToProductsList(String jsonProducts) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        //TypeReference<List<Product>> mapType = new TypeReference<List<Product>>() {};
        JsonModels jm = new JsonModels();
        List<Product> productsList = mapper.readValue(jsonProducts, new TypeReference<List<Product>>(){});//import com.fasterxml.jackson.core.type.TypeReference;
        jm.setProductsList(productsList); 
        return jm.getProductsList();
    }
    public String productFindAndDetails(JsonModels jm) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(jm);
    }
    public String productFindFailMessage(String msg) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(msg);
    }
}
