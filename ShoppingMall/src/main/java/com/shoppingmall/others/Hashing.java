/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.others;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public class Hashing {

    public static String wordHash(String pwdArray){
        //pwdArray is supposed to be an array of values the password provided by user through post method
	List<Character> hashedPwd = new ArrayList<>();

	char[] tempArray = pwdArray.toCharArray();
	//array_replace($tempArray, $pwdArray);
	//the mother Array
	char[] mArray = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','~','!','@','$','%','%','^','&','*','(',')','-','_','=','+','|',' ',',','<','.','>','/','?'};
	//stage1 key array and salting to achieve a desired length (or better salting be done at the last)
	char[] stage1 = {'d','e','B','g','h','i','f','a','C','b','c','j','D','%','^','E','&','F','k','l','G','m','n','H','s','I','t','u','v','J','w','x','K','y','z','L','0','1','M','2','3','N','4','o','O','p','q','P','r','5','Q','6','R','*','S','(',')','-','T','_','U','A','=','+','V','|',',','W','<','7','X','8','9','Y','~','!','@','Z','$','.','>','/','?'};

	char[] stage2 = {'%','^','T','&','S','*','(','U',')','R','-','V','_','=','a','W','b','Q','c','d','X','e','P','f','Y','g','O','h','i','N','j','Z','k','M','l','m','n','L','o','p','q','K','r','s','t','J','u','v','I','w','x','H','4','5','6','G','7','8','9','F','~','!','@','$','E','+','D','|',',','<','.','C','>','/','y','z','B','0','1','2','3','A','?'};

	char[] stage3 = {'g','h','A','i','j','B','k','9','C','~','!','D','@','E','$','F','%','^','G','&','*','H','(',')','I','-','J','_','=','K','+','|','L',',','M','<','.','N','a','b','O','c','d','P','e','f','>','Q','/','?','R','l','m','S','n','T','o','p','U','q','r','V','s','t','W','u','v','w','x','y','X','z','0','Y','1','2','3','Z','4','5','6','7','8'};


	//the stepwise stage arrays selector
	int[] arraySteps = {2,3,1};


	//the operation selector
	int[] addSteps = {2,5,3}; // the no. of stage arrays and the no. of elements in both arraySteps and addSteps should always be equal
	int pwdArrayLength = pwdArray.length();
	int stepCount = arraySteps.length;//count($arraySteps); // this is the stage selector
	for (int m=0; m < pwdArrayLength; m++) { 
            char tempChar = '*';
            for (int j=0; j < stepCount; j++) { 
                tempChar = tempArray[m];
                char[] currentStage;
                if(arraySteps[j]==1){
                    currentStage = stage1;
                }
                else if (arraySteps[j]==2) {
                    currentStage = stage2;
                }
                else{
                    currentStage = stage3;
                }
                int currentStageLength = currentStage.length;//count($currentStage);
                for(int n=0; n<currentStageLength; n++) {
                    if(tempChar==currentStage[n]){
                        int tempValue = (n+addSteps[j]);
                        if(tempValue >= currentStageLength){
                            int tempNo = (n+addSteps[j]);
                            n = currentStageLength - tempNo;
                        }
                        tempChar = currentStage[(addSteps[j])+n];
                        tempArray[m]=tempChar;
                        break;
                    }
                }
            }
            hashedPwd.add(tempChar);
        }
        String hash = "";
        for(int i=0; i<hashedPwd.size(); i++){
            hash = hash+hashedPwd.get(i);
        }
        return hash;
    }

    public static String securityHashForEachLogin(){
       //get current datetime string and hash that for now. in future may be something advanced
       return wordHash(DateTime.hashOfCurrentDateTime());
    }
}
