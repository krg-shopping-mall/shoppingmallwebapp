/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.others;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author Gyurme
 */
public class DateTime {
    public static String hashOfCurrentDateTime(){
        Date d = new Date();
        int dateTimeHash = d.hashCode();
        System.out.println("dateTimeHash: "+ String.valueOf(dateTimeHash));
        return String.valueOf(dateTimeHash);
    }
    public static String currentDateTime(){
        LocalDateTime lDT = LocalDateTime.now();
        DateTimeFormatter dtFormatted = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dtFormatted.format(lDT);
    }
}
