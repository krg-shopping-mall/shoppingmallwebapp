/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.services;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Gyurme
 */
public class DbConnect {
    public static Connection limitedPriviligeConnectDb(){
        String urlAndPort = "localhost:3306";
	String dbName = "shoppingmall";
	String username = "shoppingmall_limited";		
        String pwd = "pass";
	Connection conn = null;
	try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://"+urlAndPort+"/"+dbName,username,pwd);
	} 
        catch (Exception e) {
            e.printStackTrace();
        }
	return conn;
    }
    //priviliged db user supplies username only when they need
    // 'shoppingmall_priviliged'@'localhost' pass 'root'
    public static Connection priviligedConnectDb(String priviligedDbUserName, String DbPassword){
        String urlAndPort = "localhost:3306";
	String dbName = "shoppingmall";
	Connection conn = null;
	try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://"+urlAndPort+"/"+dbName,priviligedDbUserName,DbPassword);
	} 
        catch (Exception e) {
            e.printStackTrace();
        }
	return conn; 
    }
}
