/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.services;

/**
 *
 * @author Gyurme
 */
public class DbAdmin {
    String tableCreateQueries = "";
    String branch_code_all_staffs; 
    String branch_code_current_staffs; 
    String branch_code_others_transfer_details; 
    String branch_code_others_transfer_qtys; 
    String branch_code_product_stock; 
    String branch_code_sales_details; 
    String branch_code_sales_quantity; 
    String branch_code_sales_returns_details;
    String branch_code_sales_returns_products;
    String branch_code_tables_shifts; 
    String branch_code_taken_off_shelf;
    public String composeNewBranchTableCreateQueries(String newBranchCode){
        String tableCreateQueries = "";
        branch_code_all_staffs = "branch_"+newBranchCode+"_all_staffs";//
        branch_code_current_staffs = "branch_"+newBranchCode+"_current_staffs";//
        branch_code_others_transfer_details = "branch_"+newBranchCode+"_others_transfer_details";//
        branch_code_others_transfer_qtys = "branch_"+newBranchCode+"_others_transfer_qtys";//
        branch_code_product_stock = "branch_"+newBranchCode+"_product_stock";//
        branch_code_sales_details = "branch_"+newBranchCode+"_sales_details";//
        branch_code_sales_quantity = "branch_"+newBranchCode+"_sales_quantity";//
        branch_code_sales_returns_details = "branch_"+newBranchCode+"_sales_returns_details";//
        branch_code_sales_returns_products = "branch_"+newBranchCode+"_sales_returns_products";//
        branch_code_tables_shifts = "branch_"+newBranchCode+"_tables_shifts"; //
        branch_code_taken_off_shelf = "branch_"+newBranchCode+"_taken_off_shelf";//
        
        tableCreateQueries = "USE shoppingmall; "
                + "CREATE TABLE `shoppingmall`.`"+ branch_code_all_staffs +"` ( `branch_staff_id` INT(8) "
                + "UNSIGNED NOT NULL AUTO_INCREMENT ," 
                + " `password` VARCHAR(20) NULL , `from_date` DATETIME NOT NULL ,"
                + " `employee_id` INT(8) UNSIGNED NOT NULL ," 
                + " `position` VARCHAR(100) NOT NULL , PRIMARY KEY (`branch_staff_id`)) ENGINE = InnoDB; \n"
        +" CREATE TABLE `shoppingmall`.`"+ branch_code_current_staffs +"` ( `username` VARCHAR(50) NULL ,"
                + " `password` VARCHAR(50) NULL , `branch_staff_id` INT(8) UNSIGNED NOT NULL,"
                + "`loggedin_user_hash` VARCHAR(50) NULL ) ENGINE = InnoDB;"
       +" CREATE TABLE `shoppingmall`.`"+ branch_code_others_transfer_details +"` ( `transfer_id` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT ," +
                   " `transfer_date` DATETIME NOT NULL , `reciepent_branch_id` INT(5) UNSIGNED NOT NULL , " +
                   "PRIMARY KEY (`transfer_id`)) ENGINE = InnoDB; "
        +" CREATE TABLE `shoppingmall`.`"+ branch_code_others_transfer_qtys +"` ( `transfer_id` INT(8) UNSIGNED NOT NULL ," +
                    " `branch_product_id` INT(8) UNSIGNED NOT NULL , `quantity` INT(6) UNSIGNED NOT NULL ) " +
                    "ENGINE = InnoDB;"
        +" CREATE TABLE `shoppingmall`.`" + branch_code_product_stock +"` ( `product_id` INT(8) UNSIGNED NOT NULL, "
                + "`quantity` INT(6) UNSIGNED NULL , UNIQUE (`product_id`)) ENGINE = InnoDB;"
        +" CREATE TABLE `shoppingmall`.`"+ branch_code_sales_details +"` ( `sales_id` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,\n" +
                " `date_time` DATETIME NOT NULL , `branch_id` INT(5) NOT NULL , `cashier_id` INT(8) UNSIGNED NOT NULL ,\n" +
                " `customer_id` INT(8) UNSIGNED NULL , `tpin` INT(10) UNSIGNED NULL , PRIMARY KEY (`sales_id`))\n" +
                " ENGINE = InnoDB; "
        +" CREATE TABLE `shoppingmall`.`"+ branch_code_sales_quantity +"` ( `sales_id` INT(8) UNSIGNED NOT NULL ,\n" +
                " `branch_product_sn` INT(8) UNSIGNED NOT NULL , `product_id` INT(8) UNSIGNED NOT NULL ,\n" +
                " `quantity` INT(3) UNSIGNED NOT NULL , `price_during_sale` DECIMAL(5,3) NOT NULL )\n" +
                " ENGINE = InnoDB;"
        +" CREATE TABLE `shoppingmall`.`"+ branch_code_sales_returns_details +"` ( `sales_return_id` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,\n" +
                " `sales_id` INT(8) UNSIGNED NOT NULL , `return_date` DATETIME NOT NULL ,\n" +
                " `handling_staff_id` INT(8) UNSIGNED NOT NULL , PRIMARY KEY (`sales_return_id`)) ENGINE = InnoDB;"
       +" CREATE TABLE `shoppingmall`.`"+ branch_code_sales_returns_products +"` ( `return_id` INT(8) UNSIGNED NOT NULL , \n" +
                  "`product_id` INT(8) UNSIGNED NOT NULL , `return_quantity` INT(3) UNSIGNED NOT NULL , \n" +
                  "`replacement_quantity` INT(3) UNSIGNED NOT NULL ) ENGINE = InnoDB;"
        +" CREATE TABLE `shoppingmall`.`"+ branch_code_tables_shifts +"` ( `sn` INT(8) UNSIGNED NOT NULL , "
                + "`name` VARCHAR(100) NOT NULL , `from_date` DATETIME NOT NULL , `till_date` DATETIME NOT NULL ) ENGINE = InnoDB;"
        +" CREATE TABLE `shoppingmall`.`"+ branch_code_taken_off_shelf +"` ( `date_time` DATETIME NOT NULL ,\n" +
                " `branch_product_sn` INT(8) UNSIGNED NOT NULL , `quantity` INT(5) UNSIGNED NOT NULL )\n" +
                " ENGINE = InnoDB;\n";
        return tableCreateQueries;
    }
    public String branchTableRelationshipQueries(String newBranchCode){
        String relationshipQueries = " ALTER TABLE `"+ branch_code_all_staffs +"` ADD FOREIGN KEY (`employee_id`) \n" +
            "REFERENCES `employees`(`employee_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;"+
        " ALTER TABLE `"+ branch_code_current_staffs +"` ADD FOREIGN KEY (`branch_staff_id`) \n" +
            "REFERENCES `"+ branch_code_all_staffs +"`(`branch_staff_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_others_transfer_details +"` ADD FOREIGN KEY (`reciepent_branch_id`) \n" +
            "REFERENCES `branches`(`branch_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_others_transfer_qtys +"` ADD FOREIGN KEY (`transfer_id`) \n" +
            "REFERENCES `"+ branch_code_others_transfer_details +"`(`transfer_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;\n" +
            "ALTER TABLE `"+ branch_code_others_transfer_qtys +"` ADD FOREIGN KEY (`branch_product_id`) \n" +
            "REFERENCES `"+ branch_code_product_stock +"`(`product_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_product_stock +"` ADD FOREIGN KEY (`product_id`) REFERENCES `product_details`(`id`) "
                + "ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_sales_details +"` ADD FOREIGN KEY (`cashier_id`) \n" +
            "REFERENCES `"+ branch_code_all_staffs +"`(`branch_staff_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; \n" +
            "ALTER TABLE `"+ branch_code_sales_details +"` ADD FOREIGN KEY (`customer_id`) \n" +
            "REFERENCES `card_holding_customers`(`customer_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_sales_quantity +"` ADD FOREIGN KEY (`branch_product_sn`)\n" +
            " REFERENCES `"+branch_code_product_stock+"`(`product_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;\n" +
            " ALTER TABLE `"+branch_code_sales_quantity+"` ADD FOREIGN KEY (`sales_id`)\n" +
            " REFERENCES `"+branch_code_sales_details+"`(`sales_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_sales_returns_details +"` ADD FOREIGN KEY (`sales_id`) \n" +
            "REFERENCES `"+branch_code_sales_details+"`(`sales_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; \n" +
            "ALTER TABLE `"+branch_code_sales_returns_details+"` ADD FOREIGN KEY (`handling_staff_id`) \n" +
            "REFERENCES `"+branch_code_all_staffs+"`(`branch_staff_id`) ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_sales_returns_products +"` ADD FOREIGN KEY (`return_id`) \n" +
            "REFERENCES `"+branch_code_sales_returns_details+"`(`sales_return_id`) \n" +
            "ON DELETE RESTRICT ON UPDATE RESTRICT;\n" +
            "ALTER TABLE `"+branch_code_sales_returns_products+"` ADD FOREIGN KEY (`product_id`)\n" +
            " REFERENCES `product_details`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; "+
        " ALTER TABLE `"+ branch_code_taken_off_shelf +"` ADD FOREIGN KEY (`branch_product_sn`) \n" +
            "REFERENCES `"+branch_code_product_stock+"`(`product_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;";
        return relationshipQueries;
    }
}
