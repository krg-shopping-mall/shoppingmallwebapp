/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.services;

import com.shoppingmall.daoimpls.AuthenticationDAOImpl;
import com.shoppingmall.daos.AuthenticationDAO;
import com.shoppingmall.others.Hashing;

/**
 *
 * @author Gyurme
 */
public class Authentication {
    private static String getHashedPwdFromDb(String branch, String userPosition, String username){
        AuthenticationDAO aDao = new AuthenticationDAOImpl();
        return aDao.hashedPwdFromDb(branch, userPosition, username);
    }
    //user login for admin, cashier and others
    public String login(String branch, String userPosition, String username, String pwd){
        System.out.println(Hashing.wordHash(pwd));
        String generatedHash = "";
        //look for position and username and get hashed pwd from db;
        String hashedPwdFromDb = getHashedPwdFromDb(branch,userPosition,username);
        //hash pwd
        String hashedProvidedPwd = Hashing.wordHash(pwd);
        //compare hashes;
        if(hashedProvidedPwd.equals(hashedPwdFromDb)){
            AuthenticationDAO aDao = new AuthenticationDAOImpl();
            String loginHash = Hashing.securityHashForEachLogin();
            int hashDbRecordSuccess = aDao.recordGeneratedLoginHashForUser(branch, userPosition, username, loginHash);
            if(hashDbRecordSuccess > 0){
                generatedHash = loginHash;
            }   
        }
        return generatedHash;
    }
    
    //authentication before  each database manipulation
    public boolean hashCompare(String branch, String userPosition, String username, String providedHash){
        String hashFromDb = getHashFromDb(branch,userPosition,username);
        return hashFromDb.equals(providedHash);
    }
    
    //fetch hash for individual user from Db;
    private String getHashFromDb(String branch, String userPosition, String username){
        AuthenticationDAO aDao = new AuthenticationDAOImpl();
        return aDao.getUserLoggedinHash(branch, userPosition, username);
    }
    
    //the following are new methods written after new db model introduced
    
    //check the assignment existience and fetch details from db:
    
}
