/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.shoppingmall.apis.JsonHandler;
import com.shoppingmall.daoimpls.InventoryDAOImpl;
import com.shoppingmall.daoimpls.ProductDAOImpl;
import com.shoppingmall.daoimpls.SalesDAOImpl;
import com.shoppingmall.daos.InventoryDAO;
import com.shoppingmall.daos.ProductDAO;
import com.shoppingmall.daos.SalesDAO;
import com.shoppingmall.models.JsonModels;
import com.shoppingmall.models.Product;
import com.shoppingmall.models.SalesMetaData;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public class Sales {
    private static final String SALES_RECORDING_SUCCESS = "total success"; //all steps successful
    private static final String FAILURE_A = "Error Code: 012. Contact Operator immediately"; //failed in creating metaData
    private static final String FAILURE_B = "Error Code: 0132. Contact Operator immediately"; //products recording for particular sale failed
    private static final String FAILURE_C = "Error code: 0135."; //inventory manipulation failure
    //record individual sale from (entered by front end user from counter)
    public String recordPQtyOfEachSale(SalesMetaData salesMetaData, List<Product> productIdsQtys){
        SalesDAO sDao = new SalesDAOImpl();
        InventoryDAO iDao = new InventoryDAOImpl();
        String messageToCashier;
        //record Sales metadata and also generate salesId
        salesMetaData.setSalesId(sDao.recordSalesNonproductDetails(salesMetaData));
        if(salesMetaData.getSalesId() != 0){
            //record products and quantities of each sale
            int productRecSuccessCount = sDao.recordSalesProductRecord(salesMetaData, productIdsQtys);
            if(productRecSuccessCount == productIdsQtys.size()){
                //decrease sold items from inventory
                int inventoryDecreaseSuccess = iDao.decreaseFromInventory(salesMetaData.getBranchId(), productIdsQtys);
                if(inventoryDecreaseSuccess == productIdsQtys.size()){
                    //transaction Complete success message
                    messageToCashier = SALES_RECORDING_SUCCESS;
                }
                else{
                    messageToCashier = FAILURE_C;
                }
            }
            else{
                messageToCashier = FAILURE_B;
            }
        }
        else{
            messageToCashier = FAILURE_A;
        }
        
        return messageToCashier;
    }
    private Long generateAndRecordSalesMeta(SalesMetaData salesData){
        SalesDAO sDao = new SalesDAOImpl();
        Long saleId = sDao.recordSalesNonproductDetails(salesData);
        return saleId;
    }
    public String productFindAndDetails(int productId) throws JsonProcessingException{
        ProductDAO pDao = new ProductDAOImpl();
        JsonModels jm = new JsonModels();
        String jsonProductFindAndDetails = "{'productFindMessage':'notfound'}";
        Product p = pDao.getProductDetails(productId);
        //jm.setProductDetails();
        JsonHandler jh = new JsonHandler();
        if(p.getProductId() >= 1){
            jm.setProductFindMessage("found");
            jm.setProductDetails(p);
            jsonProductFindAndDetails = jh.productFindAndDetails(jm);
        }
        else if(p.getProductId() == 0){
            jm.setProductFindMessage("notfound");
            System.out.println("0 condition");
            jsonProductFindAndDetails = jh.productFindAndDetails(jm);
        }
        else{
            //to be handled later
        }
        return jsonProductFindAndDetails;
    }
    
}
