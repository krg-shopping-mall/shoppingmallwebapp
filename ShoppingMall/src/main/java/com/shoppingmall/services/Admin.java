/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.services;

import com.shoppingmall.daoimpls.AdminDAOImpl;
import com.shoppingmall.daos.AdminDAO;
import com.shoppingmall.models.Branch;
import com.shoppingmall.models.Employee;
import com.shoppingmall.models.Product;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gyurme
 */
public class Admin {
    
    //create user -> those who use front end to record sales
    
    //delete user
    
    //edit user
    
    //store TPIN (like pan) //tpin used for tax reduction and all do not need it.  
    
    
    //by self -> block, unblock users
    
    //by self -> create branch and related tables and relationships
    public String createNewBranchAndTables(String dbPrivligedUName, String dbPass){
        AdminDAO aDao = new AdminDAOImpl();
        Branch branch = new Branch();
        String successMessage;
        branch.setBranchName("trialBranch1");
        branch.setBranchAddress("trialAddress1");
        branch.setAssigned_admin(1);
        Connection conn = DbConnect.priviligedConnectDb(dbPrivligedUName, dbPass);
        int branchId = aDao.recordNewBranchDetails(branch, conn);
        successMessage = aDao.createNewBranchTables(String.valueOf(branchId), conn);
        try {
            conn.close();
        } catch (SQLException ex) {
            //Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return successMessage; //for now sql queries. In the future specific database occurances and others.
    }
    public String AddEmployee(Employee emp){
        return "";
    }
    
}
