/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.services;

import com.shoppingmall.daoimpls.InventoryDAOImpl;
import com.shoppingmall.daos.InventoryDAO;
import com.shoppingmall.models.Product;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public class InventoryMgmt {
    //add items to inventory
    public String addToInventory(int branchId, List<Product> product){
        return "";
    }
    
    //remove items from inventory
    public String removeFromInventory(int branchId, List<Product> product){
        return "";
    }
    
    //transfer item to another warehouse (branch)
    public String transferToOtherBranch(int senderBranch, int reciepentBranch, List<Product> products){
        String removeSuccess = removeFromInventory(senderBranch,products);
        //record transfer info to database
        InventoryDAO iDao = new InventoryDAOImpl();
        iDao.transferToOtherBranch(senderBranch, reciepentBranch, products);
        return "";
    }
    
    //revoke damaged or expired items and handling sales returns due to defects or policies:
        //??????
        //????
    //Register New product -> for both center and branch
    public String RegisterNewProduct(Product product){
        return "";
    }
}
