/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daoimpls;

import com.shoppingmall.daos.AuthenticationDAO;
import com.shoppingmall.services.DbConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Gyurme
 */
public class AuthenticationDAOImpl implements AuthenticationDAO{

    @Override
    public String hashedPwdFromDb(String employee_id) {
        String hashedPwdFromDb = "";
        String sql = "SELECT pwd FROM employee WHERE employee_id=?";
        Connection conn = DbConnect.limitedPriviligeConnectDb();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, employee_id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                hashedPwdFromDb = rs.getString(1);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return hashedPwdFromDb;
    }
    //assign_id will be needed for this
    @Override
    public int recordGeneratedLoginHashForUser(String branch, String userPosition, String username, String loginHash) {
        //for now position not used
        String sql = "update branch_"+branch+"_current_staffs set loggedin_user_hash = ? where username = ?;"; //the update statement
        Connection conn = DbConnect.limitedPriviligeConnectDb();
        int successRowCount = -1;
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, loginHash);
            ps.setString(2, username);
            successRowCount = ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return successRowCount;
    }
    //assign_id will be needed for this
    @Override
    public String getUserLoggedinHash(String branch, String userPosition, String username) {
        String loggedinHash = "";
        //userPosition not used for now
        String sql = "select loggedin_user_hash from branch_"+branch+"_current_staffs where username=?";
        Connection conn = DbConnect.limitedPriviligeConnectDb();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                loggedinHash = rs.getString(1);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return loggedinHash;
    }

}
