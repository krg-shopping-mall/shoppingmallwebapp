/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daoimpls;

import com.shoppingmall.daos.AdminDAO;
import com.shoppingmall.models.Branch;
import com.shoppingmall.services.DbAdmin;
import com.shoppingmall.services.DbConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Gyurme
 */
public class AdminDAOImpl implements AdminDAO{

    @Override
    public int recordNewBranchDetails(Branch branch, Connection conn) {
        String sql = "insert into branches(branch_name,branch_address) values(?,?)";
        String selectSql = "SELECT branch_id, branch_name, location FROM branches WHERE branch_id =(SELECT MAX(branch_id) FROM branches);";
        int rowsAffected = -1;
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, branch.getBranchName());
            ps.setString(2, branch.getBranchAddress());
            //ps.setString(3, String.valueOf(branch.getAssigned_admin()));
            rowsAffected = ps.executeUpdate(); //returns no of rows affected for DML statements
            if(rowsAffected > 0){
                ResultSet rs = ps.executeQuery(selectSql);
                while(rs.next()){
                    if(branch.getBranchName().equals(rs.getString("name"))){
                        branch.setBranchId(rs.getInt("branch_id"));
                    }                  
                }
            }
            else{
                branch.setBranchId(-1);
            }
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return branch.getBranchId();
    }

    @Override
    public String createNewBranchTables(String branchId, Connection conn) {
        DbAdmin da = new DbAdmin();
        String branchTableCreateQueries = da.composeNewBranchTableCreateQueries(branchId);
        String branchTableRelationsQueries = da.branchTableRelationshipQueries(branchId);
        return branchTableCreateQueries + branchTableRelationsQueries;
    }

}
