/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daoimpls;

import com.shoppingmall.daos.InventoryDAO;
import com.shoppingmall.models.Product;
import com.shoppingmall.services.DbConnect;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public class InventoryDAOImpl implements InventoryDAO{
    
    @Override
    public int addToInventory(int branchId, List<Product> productIdsAndQtys) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int decreaseFromInventory(int branchId, List<Product> productIdsAndQtys) {
        Connection conn = DbConnect.limitedPriviligeConnectDb();
        int updateSuccessCounts = 0;
        String sql = "update branch_"+branchId+"_product_stock set quantity= ((select quantity where product_id=?) - ?) where product_id=?;";
        //no need to specify two table names twice in mysql
        for(Product p : productIdsAndQtys){
            try{
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setLong(1, p.getProductId());
                ps.setBigDecimal(2, p.getQuantity());
                ps.setLong(3, p.getProductId());
                int updateSuccess = ps.executeUpdate();
                if(updateSuccess == 1){
                    updateSuccessCounts += 1;
                }
                else{
                    break;
                }
            }
            catch(SQLException e){
                e.printStackTrace();
                break;
            }
        }
        return updateSuccessCounts;
    }

    @Override
    public int addDamagedList(int branchId, List<Product> productIdsAndQtys) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int transferToOtherBranch(int senderBranchId, int reciepentBranchId, List<Product> productsIdQty) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
