/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daoimpls;

import com.shoppingmall.daos.SalesDAO;
import com.shoppingmall.models.Product;
import com.shoppingmall.models.SalesMetaData;
import com.shoppingmall.services.DbConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author Gyurme
 */
public class SalesDAOImpl implements SalesDAO{

    @Override
    public long recordSalesNonproductDetails(SalesMetaData salesData) {
        long generatedSaleId = 0;
        String sql = "insert into branch_"+salesData.getBranchId()+"_sales_details(date_time,cashier_id,customer_id,tpin)"
                + "values('"+salesData.getDateTime()+"',"+salesData.getCashierId()+","+salesData.getCustomerId()+","+salesData.getTPIN()+");";
        Connection conn = DbConnect.limitedPriviligeConnectDb();
        
        try{
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setString(1, salesData.getDateTime());
//            ps.setLong(2, salesData.getCashierId());
//            ps.setLong(3, salesData.getCustomerId());
//            ps.setLong(4, salesData.getTPIN());
//            System.out.println(ps);
//            //the following two lines required to get result from preparedstatement
//            ps.execute();
            Statement st = conn.createStatement();
            if(st.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS) > 0){
                ResultSet rs = st.getGeneratedKeys();
                while(rs.next()){
                    generatedSaleId = rs.getLong(1);
                    System.out.println("Sale Id: " + generatedSaleId);
                }
            }
            st.close();
            conn.close();

        }catch(SQLException e){
            e.printStackTrace();
        }
        return generatedSaleId;
    }

    @Override
    public SalesMetaData getSalesNonProductDetails(int branchId, long salesId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int recordSalesProductRecord(SalesMetaData smd, List<Product> productIdAndQty) {
        Connection conn = DbConnect.limitedPriviligeConnectDb();
        String sql = "insert into branch_"+smd.getBranchId()+"_sales_quantity (sales_id,product_id,quantity,price_during_sale)values(?,?,?,?);";
        int successCount = 0;
        for(int i=0; i < productIdAndQty.size(); i++){
         try{
             Product p = productIdAndQty.get(i);
             PreparedStatement ps = conn.prepareStatement(sql);
             ps.setLong(1, smd.getSalesId());
             ps.setLong(2, p.getProductId());
             ps.setBigDecimal(3, p.getQuantity());
             ps.setBigDecimal(4, p.getPrice());
             int j = ps.executeUpdate();
             if(j>0){
                 successCount += 1;
             }
             else{
                 break;
             }
         }   catch(SQLException e){
             e.printStackTrace();
         }
        }
        return successCount;
    }

    @Override
    public List<Product> productsAndQtysForSale(SalesMetaData smd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SalesMetaData> allSalesMetaSince(String fromDate, int noOfRowsPerPage, long previouslyViewedTillRow) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
