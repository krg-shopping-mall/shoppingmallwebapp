/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.daoimpls;

import com.shoppingmall.daos.ProductDAO;
import com.shoppingmall.models.Product;
import com.shoppingmall.services.DbConnect;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gyurme
 */
public class ProductDAOImpl implements ProductDAO{

    @Override
    public int addCategory(String Category) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int editCategory(long id, String Category) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int addProduct(Product pd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int editProduct(long productId, Product pd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Product getProductDetails(int productId) {
        Product p = new Product();
        //............
        Connection conn = DbConnect.limitedPriviligeConnectDb();
        String sql = "select * from product_details where id="+productId;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                p.setProductId(productId);
                p.setProductName(rs.getString("name"));
                p.setPrice(rs.getBigDecimal("price"));               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return p;
    }
    
}
