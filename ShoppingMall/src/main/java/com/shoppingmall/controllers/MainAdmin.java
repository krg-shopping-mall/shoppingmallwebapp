/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.controllers;

import com.shoppingmall.services.Admin;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gyurme
 */
@WebServlet(name = "MainAdmin", urlPatterns = {"/mainadmin"})
public class MainAdmin extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("login_type", "main_admin_login");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String requestName = request.getParameter("request_name");
        //this creates branch. to be changed later
        if(requestName.equals("main_admin_login")){
            String userName = request.getParameter("username");
            String pass = request.getParameter("password");
            System.out.println(userName + "  pass: " + pass);
            Admin admin = new Admin();
            String successMessage = admin.createNewBranchAndTables(userName, pass);
            PrintWriter out = response.getWriter();
            out.write("Run these queries in your db: \n\n"+ successMessage);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
