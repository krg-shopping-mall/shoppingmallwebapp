/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.controllers;

import com.shoppingmall.apis.JsonHandler;
import com.shoppingmall.models.Employee;
import com.shoppingmall.models.Product;
import com.shoppingmall.models.SalesMetaData;
import com.shoppingmall.others.DateTime;
import com.shoppingmall.services.Authentication;
import com.shoppingmall.services.Sales;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gyurme
 */
@WebServlet(name = "RecordSale", urlPatterns = {"/billing"})
public class RecordSale extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("login_type", "cashier_login");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String requestName = request.getParameter("request_name");
        Authentication auth = new Authentication();
        JsonHandler jh = new JsonHandler();
        Sales sales = new Sales();
        //System.out.println(requestName);
        if(requestName.equals("cashier_login")){
            String cashierUName = request.getParameter("username");
            String cashierPwd = request.getParameter("password");
            String cashierBranch = request.getParameter("branch_id");
            String generatedLoggedinHash = auth.login(cashierBranch, "cashier", cashierUName, cashierPwd);
            System.out.println(generatedLoggedinHash);
            if((generatedLoggedinHash.equals("")) == false){
                //do the required processes
                request.setAttribute("loggedInUserType", "cashier");
                request.setAttribute("loggedInUserName",cashierUName);
                request.setAttribute("loggedInUserBranch", cashierBranch);
                request.setAttribute("loggedInUserHash", generatedLoggedinHash);
                request.getRequestDispatcher("salesRecordPage.jsp").forward(request, response);
            }
            else{
                //again login page
                request.setAttribute("loginFailedMessage", "Wrong Username or Password. Please try again");
                request.setAttribute("login_type", "cashier_login");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        }
        else if(requestName.equals("product_details")){
            String cashierUName = request.getParameter("loggedin_cashier");
            String cashierBranch = request.getParameter("loggedin_user_branch");
            String loggedinHash = request.getParameter("loggedin_user_hash");
            int productCode = Integer.parseInt(request.getParameter("product_code"));
            if(auth.hashCompare(cashierBranch, "cashier", cashierUName, loggedinHash)){
                PrintWriter out = response.getWriter();
                out.print(sales.productFindAndDetails(productCode));
            }
            else{
                request.setAttribute("loginFailedMessage", "You are not logged in or you are logged in somewhere else");
                request.setAttribute("login_type", "cashier_login");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        }
        else if(requestName.equals("record_sale")){
            String productsAndQtysJson = request.getParameter("products_and_qtys");
            List<Product> products = jh.jsonToProductsList(productsAndQtysJson);
            SalesMetaData smd = new SalesMetaData();
            smd.setBranchId(Integer.parseInt(request.getParameter("loggedin_user_branch")));
            smd.setCashierId(Long.parseLong("2")); //later bring it from db
            smd.setCustomerId(Long.parseLong("0"));
            smd.setTPIN(Long.parseLong("0"));
            smd.setDateTime(DateTime.currentDateTime());
            String successMsg = sales.recordPQtyOfEachSale(smd, products);
            PrintWriter out = response.getWriter();
            out.print(successMsg);
        }
        else{
            System.out.println("Something looks wrong with sending request to this page");
        }
    }

    @Override
    public String getServletInfo() {
        return "Cashier Page for billing";
    }// </editor-fold>

}
