/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shoppingmall.controllers;

import com.shoppingmall.apis.TrialJsonReqFromServlet;
import com.shoppingmall.services.Admin;
import com.shoppingmall.services.Authentication;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gyurme
 */
//admin for each branch
@WebServlet(name = "AdminController", urlPatterns = {"/admin"})
public class AdminController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("login_type", "branch_admin_login");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String requestName = request.getParameter("request_name");
        //login and initial page
        if(requestName.equals("branch_admin_login")){
            String username = request.getParameter("username");
            String pass = request.getParameter("password");
            String branchCode = request.getParameter("branch_id");
            //if current employee exists in the branch
            if(username.equals("trial") && pass.equals("trial")){
                //authenticate branch admin
                Authentication auth = new Authentication();
                String loginHash = auth.login(branchCode, "branch_admin", username, pass);
                request.getRequestDispatcher("branchAdminPage.jsp").forward(request, response);
            }else{
                request.setAttribute("loginFailedMessage", "Wrong username or password or you are not permitted in this branch");
                request.setAttribute("login_type", "branch_admin_login");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
            
        }
        else if(requestName.equals("json_from_another_server")){
            TrialJsonReqFromServlet req = new TrialJsonReqFromServlet();
            String responseJson = req.trialRequest("regularReciepents");
            PrintWriter out = response.getWriter();
            out.print(responseJson);
        }
        //products
        //add
        else if(requestName.equals("add_to_inventory")){
            
        }
        //remove from inventory for damage and expiry
        else if(requestName.equals("remove_from_inventory")){
            
        }
        //transfering from this branch to next
        else if(requestName.equals("transfer_to_branch")){
            
        }
        //register product to branch registered to central db
        else if(requestName.equals("register_product_at_branch")){
            
        }
        //list all products
        else if(requestName.equals("list_all_from_stock")){
            
        }
        
        //staffs
        else{
            
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
